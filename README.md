# docker-symfony

Docker Compose configuration for Symfony 4 project to enable:

* PHPEearth PHP 7.2
* Nginx
* MySQL 5.7
* Adiminer

## Symfony extra components

Execute *extra.sh* to install extra symfony components

* symfony/web-server-bundle --dev
* require symfony/maker-bundle --dev
* require symfony/requirements-checker
* require annotations

## Author

**Adm. Marcus Vinícius R G Cardoso**
(Mvinicius Consultoria)

- E-mail: <mailto:marcus@mviniciusconsultoria.com.br>
- Site: <http://mviniciusconsultoria.com.br>

## Copyright an Licence

2013-2019 MVinicius Consultoria, by [MIT License](http://opensource.org/licenses/MIT).

Documentation under [MIT Licence](http://opensource.org/licenses/MIT).
